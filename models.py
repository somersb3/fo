from django.utils.translation import ugettext_lazy as _ 
from django import forms
from django.core.validators import validate_email
from django.forms.widgets import *
from django.core.mail import send_mail, BadHeaderError

# A simple contact form with four fields.
class ContactForm(forms.Form):
    name = forms.CharField(required=True,initial='')
    email = forms.EmailField(required=True, validators=[validate_email], initial='',
        error_messages={'invalid': _(u'Enter a valid e-mail address.')})
    topic = forms.CharField(required=True,initial='')
    phone = forms.CharField(required=True,initial='')
    message = forms.CharField(required=True, widget=Textarea(), initial='')
