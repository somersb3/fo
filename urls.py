from django.conf import settings
from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
from chbfo.admin import admin
#admin.autodiscover()

urlpatterns = patterns('', ('', include('chbfo.urls')))


if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
       url(r'^rosetta/', include('rosetta.urls')),
    )

urlpatterns += staticfiles_urlpatterns()
urlpatterns += patterns('', url(r'^i18n/', include('django.conf.urls.i18n')),)
urlpatterns += patterns('', url(r'^uudised/i18n/', include('django.conf.urls.i18n')),)
urlpatterns += patterns('', url(r'^admin/', include(admin.urls)),)
