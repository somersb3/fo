from transmeta import TransMeta
from django.utils.html import linebreaks
from django.utils import timezone
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.core.validators import validate_email
from django.forms.widgets import *
from django.core.mail import send_mail, BadHeaderError

# A simple contact form with four fields.
class ContactForm(forms.Form):
    name = forms.CharField(required=True,initial='')
    email = forms.EmailField(required=True, validators=[validate_email], initial='',
            error_messages={'invalid': _(u'Enter a valid e-mail address.')})
    topic = forms.CharField(required=True,initial='')
    phone = forms.CharField(required=True,initial='')
    message = forms.CharField(required=True, widget=Textarea(), initial='')


DRAFT = 0
HIDDEN = 1
PUBLISHED = 2
STATUS_CHOICES = ((HIDDEN, _('Peidetud')), (PUBLISHED, _('Avalik')))
class Entry(models.Model):
    title = models.CharField(_('Pealkiri'), max_length=255)
    content = models.TextField(_('Sisu'))
    author = models.ForeignKey(User, verbose_name=_('Autor'),
                                    blank=False, null=False)
    status = models.IntegerField(choices=STATUS_CHOICES, default=DRAFT)
    creation_date = models.DateTimeField(_('Kuupaev'),
                            default=timezone.now)

    @property
    def html_content(self):
        return linebreaks(self.content)

    __metaclass__ = TransMeta
    class Meta:
        translate = ('content', 'title', )

