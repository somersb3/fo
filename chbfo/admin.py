from django.contrib import admin as ADMIN
from chbfo.models import Entry
class CustomAdminSite(ADMIN.sites.AdminSite):
    pass

admin = CustomAdminSite(name='admin')

class EntryAdmin(ADMIN.ModelAdmin):
    list_display = ('title', 'author', 'creation_date')
    '''
    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(BookAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        db_fieldname = canonical_fieldname(db_field)
        if db_fieldname == '':
            # this applies to all description_* fields
            field.widget = MyCustomWidget()
        elif field.name == 'body_es':
            # this applies only to body_es field
            field.widget = MyCustomWidget()
        return field
    '''

admin.register(Entry, EntryAdmin)

