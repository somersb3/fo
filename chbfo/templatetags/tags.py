from django import template
from chbfo.models import Entry, HIDDEN
register = template.Library()

@register.inclusion_tag('recent_entries.html')
def recent_entries():
     result = Entry.objects.all().exclude(status=HIDDEN).order_by('-creation_date')[:5]
     return {'entries': result}

@register.inclusion_tag('all_entries.html')
def all_entries():
     result = Entry.objects.all().exclude(status=HIDDEN).order_by('-creation_date')
     return {'entries': result}
