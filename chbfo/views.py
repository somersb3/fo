
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from models import ContactForm
from django.template import RequestContext, Context
from django import forms
from django.forms.widgets import *
from django.core.mail import send_mail, BadHeaderError
from django.shortcuts import render_to_response

def home(request):
    return render_to_response('home.html', {}, context_instance=RequestContext(request))

def history(request):
    return render_to_response('history.html', {}, context_instance=RequestContext(request))

def partners(request):
    return render_to_response('partners.html', {}, context_instance=RequestContext(request))

def team(request):
    return render_to_response('team.html', {}, context_instance=RequestContext(request))

def services(request):
    return render_to_response('services.html', {}, context_instance=RequestContext(request))

def solutions(request):
    return render_to_response('solutions.html', {}, context_instance=RequestContext(request))

def contact(request):
    return render_to_response('contact.html', {}, context_instance=RequestContext(request))

def clientinfo(request):
    return render_to_response('clientinfo.html', {}, context_instance=RequestContext(request))

def laws(request):
    return render_to_response('laws.html', {}, context_instance=RequestContext(request))

def requirements(request):
    return render_to_response('requirements.html', {}, context_instance=RequestContext(request))

def links(request):
    return render_to_response('links.html', {}, context_instance=RequestContext(request))

def thankyou(request):
    return render_to_response('thankyou.html', {}, context_instance=RequestContext(request))

def contact(request):
	if request.method == 'POST': 
		form = ContactForm(request.POST) 
		if form.is_valid(): 
			cd = form.cleaned_data 
			send_mail(cd['topic'], cd['message'], cd.get('email', 'noreply@example.com'), ['ivo@chb.ee'], ) 
			return render_to_response('thankyou.html', {'form': form}, context_instance=RequestContext(request)) 
	else: 
		form = ContactForm() 
	return render_to_response('contact.html', {'form': form}, context_instance=RequestContext(request)) 
