from django.conf import settings
from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:

urlpatterns = patterns('',
	url(r'^$', 'views.home'),

    url(r'^uudised$', 'views.news', name='news'),
    url(r'^avaleht$', 'views.home'),
    url(r'^ajalugu$', 'views.history'),
    url(r'^partnerid$', 'views.partners'),
    url(r'^inimesed$', 'views.team'),
    url(r'^teenused$', 'views.services'),
    url(r'^lahendused$', 'views.solutions'),
    url(r'^maaklerileping$', 'views.clientinfo'),
    url(r'^kaebused$', 'views.laws'),
    url(r'^seadused$', 'views.requirements'),
    url(r'^viited$', 'views.links'),
    url(r'^kontakt$', 'views.contact'),
	url(r'^saadetud', 'views.thankyou'),
)
